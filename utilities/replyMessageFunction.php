<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Message.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/languageFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $message_uid = rewrite($_POST["message_uid"]);
    $senderUID = rewrite($_POST["sender_uid"]);
    $message_details = rewrite($_POST["message_details"]);
    $adminStatus = "REPLY";
    $userStatus = "GET";
    $updateMessageStatus = "NO";

    //for debugging
    echo "<br>";
    echo $message_uid."<br>";
    echo $senderUID."<br>";
    echo $message_details."<br>";

    if(isset($_POST['reply_sms']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($message_details)
        {
            array_push($tableName,"reply_message");
            array_push($tableValue,$message_details);
            $stringType .=  "s";
        }     
        if($adminStatus)
        {
            array_push($tableName,"admin_status");
            array_push($tableValue,$adminStatus);
            $stringType .=  "s";
        } 
        if($userStatus)
        {
            array_push($tableName,"user_status");
            array_push($tableValue,$userStatus);
            $stringType .=  "s";
        } 

        array_push($tableValue,$message_uid);
        $stringType .=  "s";
        $messageUpdated = updateDynamicData($conn,"message"," WHERE message_uid = ? ",$tableName,$tableValue,$stringType);
        
        if($messageUpdated)
        {
            // echo "success";
            // echo "<script>alert('successfully reply message!');window.location='../adminViewMessage.php'</script>"; 
            // header('Location: ../adminViewMessage.php?type=1');

            if(isset($_POST['reply_sms']))
            {   
                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database"; 
                if($updateMessageStatus)
                {
                array_push($tableName,"message");
                array_push($tableValue,$updateMessageStatus);
                $stringType .=  "s";
                } 
                array_push($tableValue,$senderUID);
                $stringType .=  "s";
                $messageStatusInUser = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                if($messageStatusInUser)
                {
                    header('Location: ../adminViewMessage.php?type=1');
                }
                else
                {
                    header('Location: ../adminViewMessage.php?type=2');
                }
            }
            else
            {
                header('Location: ../adminViewMessage.php?type=3');
            }
        }
        else
        {
            // echo "fail";
            // echo "<script>alert('fail reply message!');window.location='../adminViewMessage.php'</script>"; 
            header('Location: ../adminViewMessage.php?type=4');
        }
    }
    else
    {
        // echo "dunno";
        // echo "<script>alert('error!');window.location='../adminViewMessage.php'</script>"; 
        header('Location: ../adminViewMessage.php?type=5');
    }
}
else 
{
    // echo "gg";
    header('Location: ../index.php');
}

?>