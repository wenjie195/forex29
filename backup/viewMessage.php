<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Message.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

// $userSMSDetails = getMessage($conn," WHERE uid = ? ORDER BY date_created DESC",array("uid"),array($uid),"s");
// $userSMSDetails = getMessage($conn," WHERE uid = ? ORDER BY date_created ASC",array("uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dxforextrade88.com/viewMessage.php" />
    <meta property="og:title" content="View Message | De Xin Guo Ji 德鑫国际" />
    <title>View Message | De Xin Guo Ji 德鑫国际</title>
    <meta property="og:url" content="https://dxforextrade88.com/viewMessage.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<div class="dark-bg overflow same-padding">
	<?php include 'headerAfterLogin.php'; ?>
    <h1 class="menu-distance h1-title white-text text-center"><?php echo _VIEWMESSAGE_VIEW_ALL_MESSAGE ?></h1>
    <div class="width100 overflow blue-opa-bg padding-box radius-box">
        <table class="table-width data-table message-table">
            <?php
            $conn = connDB();
            $userSMSDetails = getMessage($conn," WHERE uid = ? ORDER BY date_created ASC",array("uid"),array($uid),"s");
            if($userSMSDetails)
            {   
                for($cnt = 0;$cnt < count($userSMSDetails) ;$cnt++)
                {
                ?>
                <thead>
                    <tr>
                        <th><?php echo $userSMSDetails[$cnt]->getReceiveSMS();?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?php echo $userSMSDetails[$cnt]->getReplySMS();?></td>
                    </tr>
                </tbody>
                <?php
                }
                ?>
            <?php
            }
            $conn->close();
            ?>
        </table>

            <?php
            $conn = connDB();
            $userSMSDetails = getMessage($conn," WHERE uid = ? ORDER BY date_created ASC",array("uid"),array($uid),"s");
            if($userSMSDetails)
            {
            ?>

                <!-- <form action="viewMessageDetails.php" method="POST"> -->
                <form action="utilities/replyMessageTwoFunction.php" method="POST">
                <!-- <form action="utilities/submitCSFunction.php" method="POST"> -->

                    <p class="input-top-text">Message</p>
                    <input class="clean de-input" type="text" placeholder="Your Message Here" id="message_details" name="message_details" required>

                    <button class="clean hover1 blue-button smaller-font" type="submit" name="sent_sms">
                        SENT
                    </button>

                </form>

            <?php
            }
            $conn->close();
            ?>

        <!-- </table> -->
	</div>
</div>
<?php include 'js.php'; ?>
</body>
</html>