<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BetStatus.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $tz = 'Asia/Kuala_Lumpur';
// $timestamp = time();
// $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
// $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
// $time = $dt->format('Y-m-d H:i:s');
// //additional hrs or mins
// $cenvertedTimeMin = date('Y-m-d H:i:s',strtotime('-11 minutes',strtotime($time)));
// $tradeDetails = getBetstatus($conn," WHERE date_created >= '".$cenvertedTimeMin."' ");

$tradeDetailsA = getBetstatus($conn," WHERE timeline = '30' ");
$tradeDetailsB = getBetstatus($conn," WHERE timeline = '60' ");
$tradeDetailsC = getBetstatus($conn," WHERE timeline = '180' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<!-- <?php //include 'meta.php'; ?> -->
    <meta property="og:url" content="https://dxforextrade88.com/adminCheckPreviousTrade.php" />
    <meta property="og:title" content="Previous Trade | De Xin Guo Ji 德鑫国际" />
    <title>Previous Trade | De Xin Guo Ji 德鑫国际</title>
   <link rel="canonical" href="https://dxforextrade88.com/adminCheckPreviousTrade.php" />
	<!-- <?php //include 'css.php'; ?> -->
</head>
<body class="body">

<?php //echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<!-- <?php //include 'adminSidebar.php'; ?> -->

<div class="next-to-sidebar">
    <h1 class="h1-title">Trade Records</h1>

    <h5 class="h1-title">Result 'WIN' is admin lose || 'LOSE' is admin win</h5>

    <h5 class="h1-title">Edited Result 'Lose' is admin win</h5>

    <div class="clear"></div>

    <div class="width100 overflow">

        <div class="input50-div">
            <h5 class="h1-title">30 sec</h5>
            <?php $conn = connDB();?>
            <table class="shipping-table">    
                <thead>
                    <tr>
                        <th>NO.</th>
                        <th>TIMELINE</th>
                        <th>CURRENCY</th>
                        <th>TRADE TYPE</th>
                        <th>AMOUNT</th>
                        <th>START RATE</th>
                        <th>END RATE</th>
                        <th>RESULT</th>
                        <th>EDITED RESULT</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($tradeDetailsA != null)
                    {
                    for($cntA = 0;$cntA < count($tradeDetailsA) ;$cntA++)
                    {?>
                    <tr>
                        <td><?php echo ($cntA+1)?></td>
                        <td><?php echo $tradeDetailsA[$cntA]->getTimeline();?> sec</td>
                        <td><?php echo $tradeDetailsA[$cntA]->getCurrency();?></td>
                        <td><?php echo $tradeDetailsA[$cntA]->getBetType();?></td>
                        <td><?php echo $tradeDetailsA[$cntA]->getAmount();?></td>
                        <td><?php echo $tradeDetailsA[$cntA]->getStartRate();?></td>
                        <td><?php echo $tradeDetailsA[$cntA]->getEndRate();?></td>
                        <td><?php echo $tradeDetailsA[$cntA]->getResult();?></td>
                        <td><?php echo $tradeDetailsA[$cntA]->getResultEdited();?></td>
                    </tr>
                    <?php
                    }
                    }
                    ?>
                </tbody>
            </table>
            <?php $conn->close();?>
        </div>

        <div class="input50-div">
            <h5 class="h1-title">60 sec</h5>
            <?php $conn = connDB();?>
            <table class="shipping-table">    
                <thead>
                    <tr>
                        <th>NO.</th>
                        <th>TIMELINE</th>
                        <th>CURRENCY</th>
                        <th>TRADE TYPE</th>
                        <th>AMOUNT</th>
                        <th>START RATE</th>
                        <th>END RATE</th>
                        <th>RESULT</th>
                        <th>EDITED RESULT</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($tradeDetailsB != null)
                    {
                    for($cntB = 0;$cntB < count($tradeDetailsB) ;$cntB++)
                    {?>
                    <tr>
                        <td><?php echo ($cntB+1)?></td>
                        <td><?php echo $tradeDetailsB[$cntB]->getTimeline();?> sec</td>
                        <td><?php echo $tradeDetailsB[$cntB]->getCurrency();?></td>
                        <td><?php echo $tradeDetailsB[$cntB]->getBetType();?></td>
                        <td><?php echo $tradeDetailsB[$cntB]->getAmount();?></td>
                        <td><?php echo $tradeDetailsB[$cntB]->getStartRate();?></td>
                        <td><?php echo $tradeDetailsB[$cntB]->getEndRate();?></td>
                        <td><?php echo $tradeDetailsB[$cntB]->getResult();?></td>
                        <td><?php echo $tradeDetailsB[$cntB]->getResultEdited();?></td>
                    </tr>
                    <?php
                    }
                    }
                    ?>
                </tbody>
            </table>
            <?php $conn->close();?>
        </div>

        <div class="input50-div">
            <h5 class="h1-title">180 sec</h5>
            <?php $conn = connDB();?>
            <table class="shipping-table">    
                <thead>
                    <tr>
                        <th>NO.</th>
                        <th>TIMELINE</th>
                        <th>CURRENCY</th>
                        <th>TRADE TYPE</th>
                        <th>AMOUNT</th>
                        <th>START RATE</th>
                        <th>END RATE</th>
                        <th>RESULT</th>
                        <th>EDITED RESULT</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($tradeDetailsC != null)
                    {
                    for($cntC = 0;$cntC < count($tradeDetailsC) ;$cntC++)
                    {?>
                    <tr>
                        <td><?php echo ($cntC+1)?></td>
                        <td><?php echo $tradeDetailsC[$cntC]->getTimeline();?> sec</td>
                        <td><?php echo $tradeDetailsC[$cntC]->getCurrency();?></td>
                        <td><?php echo $tradeDetailsC[$cntC]->getBetType();?></td>
                        <td><?php echo $tradeDetailsC[$cntC]->getAmount();?></td>
                        <td><?php echo $tradeDetailsC[$cntC]->getStartRate();?></td>
                        <td><?php echo $tradeDetailsC[$cntC]->getEndRate();?></td>
                        <td><?php echo $tradeDetailsC[$cntC]->getResult();?></td>
                        <td><?php echo $tradeDetailsC[$cntC]->getResultEdited();?></td>
                    </tr>
                    <?php
                    }
                    }
                    ?>
                </tbody>
            </table>
            <?php $conn->close();?>
        </div>

    </div>

    <div class="clear"></div>
</div>

<style>
.account-li{
	color:#bf1b37;
	background-color:white;}
.account-li .hover1a{
	display:none;}
.account-li .hover1b{
	display:block;}
</style>

<!-- <?php //include 'js.php'; ?> -->

</body>
</html>