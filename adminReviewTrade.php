<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BetStatus.php';
require_once dirname(__FILE__) . '/classes/BuySell.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$tradeDetails = getBetstatus($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dxforextrade88.com/adminReviewTrade.php" />
    <meta property="og:title" content="Review Trade Result | De Xin Guo Ji 德鑫国际" />
    <title>Review Trade Result | De Xin Guo Ji 德鑫国际</title>
    <link rel="canonical" href="https://dxforextrade88.com/adminReviewTrade.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<div class="dark-bg overflow same-padding">
	<?php include 'headerAdmin.php'; ?>
    <?php include 'sidebar.php'; ?>
    <div class="width100 same-padding2">
    <h1 class="menu-distance h1-title white-text text-center">Review Trade Result</h1>
    <h4 class="h1-title white-text text-center">Result "Lose" = Admin Win</h4>
    <h4 class="h1-title white-text text-center">Edited Result "Lose" = Admin Lose</h4>
    <div class="width100 overflow blue-opa-bg padding-box radius-box">
    <table class="table-width data-table message-table">
                    <thead>
                        <tr>
                            <!-- <th>NO</th>
                            <th class="two-white-border">SENT</th>
                            <th class="two-white-border">REPLY</th>
                            <th class="two-white-border">DATE</th>
                            <th>REPLY</th> -->
                            <th>NO</th>
                            <th class="two-white-border">TIMELINE</th>
                            <th class="two-white-border">CURRENCY</th>
                            <th class="two-white-border">TRADE TYPE</th>
                            <th class="two-white-border">AMOUNT</th>
                            <th class="two-white-border">START RATE</th>
                            <th class="two-white-border">END RATE</th>
                            <th class="two-white-border">RESULT</th>
                            <th class="two-white-border">EDITED RESULT</th>
                            <!-- <th class="two-white-border">VERIFY</th> -->
                            <th>VERIFY</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if($tradeDetails != null)
                        {
                        for($cnt = 0;$cnt < count($tradeDetails) ;$cnt++)
                        {?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $tradeDetails[$cnt]->getTimeline();?> sec</td>
                            <td><?php echo $tradeDetails[$cnt]->getCurrency();?></td>
                            <td><?php echo $tradeDetails[$cnt]->getBetType();?></td>
                            <td><?php echo $tradeDetails[$cnt]->getAmount();?></td>
                            <td><?php echo $tradeDetails[$cnt]->getStartRate();?></td>
                            <td><?php echo $tradeDetails[$cnt]->getEndRate();?></td>
                            <td><?php echo $tradeDetails[$cnt]->getResult();?></td>
                            <td><?php echo $tradeDetails[$cnt]->getResultEdited();?></td>

                            <td>                                
                                <?php
                                $editRes = $tradeDetails[$cnt]->getResultEdited();
                                if($editRes == '')
                                {
                                ?>
                                    <form action="utilities/adminEditResultFunction.php" method="POST">
                                        <input type="hidden" id="trading_uid" name="trading_uid" value="<?php echo $tradeDetails[$cnt]->getTradeUid()?>">
                                        <input type="hidden" id="trading_userid" name="trading_userid" value="<?php echo $tradeDetails[$cnt]->getUid()?>">
                                        <input type="hidden" id="trading_id" name="trading_id" value="<?php echo $tradeDetails[$cnt]->getId()?>">
                                        <input type="hidden" id="trading_amount" name="trading_amount" value="<?php echo $tradeDetails[$cnt]->getAmount()?>">
                                        <input type="hidden" id="trading_result" name="trading_result" value="<?php echo $tradeDetails[$cnt]->getResult()?>">
                                        <button class="clean hover1 blue-button smaller-font" type="submit" name="trade_uid" value="<?php echo $tradeDetails[$cnt]->getTradeUid();?>">
                                            EDIT RESULT
                                        </button>
                                    </form>
                                <?php
                                }
                                else
                                {?>
                                    <!-- EDIT AGAIN -->
                                    <form action="utilities/adminRevertEditResultFunction.php" method="POST">
                                        <input type="hidden" id="trading_uid" name="trading_uid" value="<?php echo $tradeDetails[$cnt]->getTradeUid()?>">
                                        <input type="hidden" id="trading_userid" name="trading_userid" value="<?php echo $tradeDetails[$cnt]->getUid()?>">
                                        <input type="hidden" id="trading_id" name="trading_id" value="<?php echo $tradeDetails[$cnt]->getId()?>">
                                        <input type="hidden" id="trading_amount" name="trading_amount" value="<?php echo $tradeDetails[$cnt]->getAmount()?>">
                                        <input type="hidden" id="trading_result" name="trading_result" value="<?php echo $tradeDetails[$cnt]->getResult()?>">
                                        <input type="hidden" id="trading_editresult" name="trading_editresult" value="<?php echo $tradeDetails[$cnt]->getResultEdited()?>">
                                        <button class="clean hover1 blue-button smaller-font" type="submit" name="trade_uid" value="<?php echo $tradeDetails[$cnt]->getTradeUid();?>">
                                            EDIT RESULT
                                        </button>
                                    </form>

                                <?php
                                }
                                ?>
                            </td>
                        </tr>
                        <?php
                        }
                        }
                        ?>
                    </tbody>

                </table>
    </div>
    </div>
</div>
<?php include 'js.php'; ?>
</body>
</html>