<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Message.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

// $userSMSDetails = getMessage($conn," WHERE uid = ? ORDER BY date_created DESC",array("uid"),array($uid),"s");
// $userSMSDetails = getMessage($conn," WHERE uid = ? ORDER BY date_created ASC",array("uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dxforextrade88.com/viewMessage.php" />
    <meta property="og:title" content="View Message | De Xin Guo Ji 德鑫国际" />
    <title>View Message | De Xin Guo Ji 德鑫国际</title>
    <meta property="og:url" content="https://dxforextrade88.com/viewMessage.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<div class="dark-bg overflow same-padding">
	<?php include 'headerAfterLogin.php'; ?>
    <h1 class="menu-distance h1-title white-text text-center"><?php echo _VIEWMESSAGE_VIEW_ALL_MESSAGE ?></h1>
    <div class="width100 overflow blue-opa-bg padding-box radius-box chat-div-padding">
		<div class="fix-scroll-msg">       
            <?php
            $conn = connDB();
            $userSMSDetails = getMessage($conn," WHERE uid = ? ORDER BY date_created ASC",array("uid"),array($uid),"s");
            if($userSMSDetails)
            {   
                for($cnt = 0;$cnt < count($userSMSDetails) ;$cnt++)
                {
                ?>
			<div class="user-chat-bubble"><?php echo $userSMSDetails[$cnt]->getReceiveSMS();?></div>

			<div class="admin-chat-bubble"><?php echo $userSMSDetails[$cnt]->getReplySMS();?></div>

                <?php
                }
                ?>
            <?php
            }
            $conn->close();
            ?>


            <?php
            $conn = connDB();
            $userSMSDetails = getMessage($conn," WHERE uid = ? ORDER BY date_created ASC",array("uid"),array($uid),"s");
            if($userSMSDetails)
            {
            ?>
        </div>
        
		<div class="clear"></div>
        <div class="fix-msg-box">
                <!-- <form action="viewMessageDetails.php" method="POST"> -->
                <form action="utilities/replyMessageTwoFunction.php" method="POST">
                <!-- <form action="utilities/submitCSFunction.php" method="POST"> -->

           
                    <input class="clean de-input left-msg" type="text" placeholder="Your Message Here" id="message_details" name="message_details" required>

                    <button class="clean hover1 blue-button smaller-font right-submit" type="submit" name="sent_sms">
                        SENT
                    </button>

                </form>
		</div>
            <?php
            }
            $conn->close();
            ?>

        <!-- </table> -->
	</div>
</div>
<style>
::-webkit-scrollbar {
  width: 3px;
}

/* Track */
::-webkit-scrollbar-track {
  background: #15212d; 
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: #15212d; 
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #15212d; 
}
</style>
<?php include 'js.php'; ?>
</body>
</html>